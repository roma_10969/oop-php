<?php
    
    require('animal.php');
    require('ape.php');
    require('frog.php');

    $sheep =  new Animal("Shaun");

    echo "Name : " . $sheep->name . "<br>";
    echo "legs : " . $sheep->legs . "<br>";
    echo "cold blooded : " . $sheep->cold_blooded . "<br><br>";

    $frog = new Frog("buduk");

    echo "Name : " . $frog->name . "<br>";
    echo "legs : " . $frog->legs . "<br>";
    echo "cold blooded : " . $frog->cold_blooded . "<br>";
    $frog->jump();

    $ape = new Ape("kera sakti");

    echo "Name : " . $ape->name . "<br>";
    echo "legs : " . $ape->legs . "<br>";
    echo "cold blooded : " . $ape->cold_blooded . "<br>";
    $ape->yell();